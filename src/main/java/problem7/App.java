package problem7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Write a program that will read sentences from a text file, placing each sen-
 tence in its own instance of ArrayList . (You will create a sentence object
 by adding words to it one at a time as they are read.) When a sentence has
 been completely read, add the sentence to another instance of ArrayList .
 Once you have read the entire file, you will have an instance of ArrayList
 that contains several instances of ArrayList , one for each sentence read.
 Now ask the user to enter a sentence number and a word number. Display
 the word that occurs in the given position. If the sentence number or word
 number is not valid, provide an appropriate error message.

 !!!Suppose there is no periods inside a sentence, else this problem requires
 artificial intelligence or another complex stuff :D
 *
 */
public class App
{
    private static String fileToString(File file) throws FileNotFoundException {
        String readText;
        try (Scanner input = new Scanner(file)) {
            readText = input.useDelimiter("\\Z").next();
        }
        return readText;
    }

    static List<List<String>> getSentences(String readText) {
        List<List<String>> sentences = new ArrayList<>();
        List<String> sentence = new ArrayList<>();
        String word = "";
        String symbol;

        for (int i = 0; i < readText.length(); i++) {
            symbol = String.valueOf(readText.charAt(i));

            if (symbol.matches("[a-zA-Z\\-']")) {
                word += symbol;
                if ((i + 1) == readText.length()) {
                    sentence.add(word);
                    sentences.add(sentence);
                }
            } else if (symbol.matches("[\\.?!]")) {
                if (!word.equals("")) {
                    sentence.add(word);
                    word = "";
                }
                if (!sentence.isEmpty()) {
                    sentences.add(sentence);
                }
                sentence = new ArrayList<>();
            } else if (!word.equals("")){
                sentence.add(word);
                word = "";
                if ((i + 1) == readText.length()) {
                    sentences.add(sentence);
                }
            } else {
                if ((i + 1) == readText.length() && !sentence.isEmpty()) {
                    sentences.add(sentence);
                }
            }
        }

        return sentences;
    }

    public static void main( String[] args )
    {
        try {
            File resource = new File("./text.dat");
            String readFile = fileToString(resource);
            List<List<String>> sentences = getSentences(readFile);

            Scanner enteredNumber = new Scanner(System.in);
            int sentenceNumber;
            int wordNumber;

            while (true) {
                System.out.println("There are " + sentences.size() + " sentences. Enter a sentence number:");
                try {
                    sentenceNumber = enteredNumber.nextInt();
                    if (sentenceNumber > sentences.size()) {
                        System.out.println("Please choose the number no more than " +
                                sentences.size());
                    } else if (sentenceNumber < 1) {
                        System.out.println("Error. The number can not be less than 1. Try again.");
                    } else {
                        break;
                    }
                } catch (InputMismatchException e) {
                    enteredNumber.nextLine();
                    System.out.println("Error. You can enter only a number. Try again.");
                }
            }

            while (true) {
                System.out.println("There are " + sentences.get(sentenceNumber - 1).size() + " words. " +
                        "Enter a word number:");
                try {
                    wordNumber = enteredNumber.nextInt();
                    if (wordNumber > sentences.get(sentenceNumber - 1).size()) {
                        System.out.println("There are only " + sentences.get(sentenceNumber - 1).size() + " words " +
                                "in the chosen sentence. Please choose the number in this range:");
                    } else if (wordNumber < 1) {
                        System.out.println("Error. The number can not be less than 1. Try again.");
                    } else {
                        break;
                    }
                } catch (InputMismatchException e) {
                    enteredNumber.nextLine();
                    System.out.println("Error. You can enter only a number. Try again.");
                }
            }

            System.out.println("The chosen word: " + sentences.get(sentenceNumber - 1).get(wordNumber - 1));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Sorry. There is a problem with reading the file. Program will be stopped.");
        }
    }
}
