var assert = require('assert');
var getSentences = require('../problem7');

describe('Get array of sentences from the file', function() {
  it('1. Get a certain word.', function () {
    assert.equal("jojo", getSentences.getSentences("Fghdhjs. Jkjk 45 " +
      "jojo! \"Hdhdhj\" fh-jfkerfj...")[1][1]);
  });

  it('2. Get the the number of sentences.', function () {
    assert.equal(2, getSentences.getSentences("gdffd... hdfdf").length);
  });
});
