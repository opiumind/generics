function getSentences(readText) {
  let sentences = [],
    sentence = [],
    word = "",
    symbol = "";

  for (let i = 0; i < readText.length; i++) {
    symbol = readText[i];

    if (symbol.match(/[a-zA-Z\-']/)) {
      word += symbol;
      if ((i + 1) === readText.length) {
        sentence.push(word);
        sentences.push(sentence);
      }
    } else if (symbol.match(/[\.?!]/)) {
      if (word) {
        sentence.push(word);
        word = "";
      }
      if (sentence && sentence.length) {
        sentences.push(sentence);
      }
      sentence = [];
    } else if (word){
      sentence.push(word);
      word = "";
      if ((i + 1) === readText.length) {
        sentences.push(sentence);
      }
    } else {
      if ((i + 1) === readText.length && sentence && sentence.length) {
        sentences.push(sentence);
      }
    }
  }

  return sentences;
}

function main() {
  let fs = require('fs'),
    readFileStr = "",
    sentences = [];

  fs.readFile('../../../text.dat', {encoding: 'utf-8'}, function (err, data) {
    if (!err) {
      readFileStr = data;
      sentences = getSentences(readFileStr);

      let sentenceNumber, wordNumber;

      let stdin = process.openStdin();

      let result = new Promise((resolve) => {
        console.log("There are " + sentences.length + " sentences. Enter a sentence number:");
        stdin.addListener("data", (d) => {
          let input1 = d.toString().trim();

          if (input1.match(/\d+/) && input1 > 0) {

            if (input1 <= sentences.length) {
              sentenceNumber = input1;
              stdin.removeAllListeners('data');
              resolve(input1);
            } else {
              console.log("There are only " + sentences.length + " sentences. Please choose the number in this range:");
            }

          } else {
            console.log("Error. You can enter only a number which must be greater than 0. Try again.");
          }

        });
      })
        .then(() => {
          console.log("There are " + sentences[sentenceNumber - 1].length + " words. Enter a word number:");

          return new Promise((resolve) => {
            stdin.addListener("data", function (d) {
              let input2 = d.toString().trim();

              if (input2.match(/\d+/) && input2 > 0) {
                if (input2 <= sentences[sentenceNumber - 1].length) {
                  wordNumber = input2;
                  resolve();
                } else {
                  console.log("There are only " + sentences[sentenceNumber - 1].length + " words in the chosen sentence. " +
                    "Please choose the number in this range:");
                }
              } else {
                console.log("Error. You can enter only a number which must be greater than 0. Try again.");
              }
            });
          })
        })
        .then(() => {
          console.log("The chosen word: " + sentences[sentenceNumber - 1][wordNumber - 1]);
          process.exit(0);
        })
        .catch(e => console.log(e));

    } else {
      console.log(err);
      console.log("Sorry. There is a problem with reading the file. Program will be stopped.");
      return false;
    }
  });
}

main();

module.exports = {
  getSentences: getSentences
};