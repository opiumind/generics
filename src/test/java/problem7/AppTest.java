package problem7;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testGetSentences() {
        assertEquals("Wrong size.", 0, App.getSentences("1010").size());
        assertEquals("Wrong size.", 1, App.getSentences("Great!").size());
        assertEquals("Wrong size.", 1, App.getSentences("Great.").size());
        assertEquals("Wrong size.", 1, App.getSentences("Great? 5757").size());
        assertEquals("Wrong size.", 2, App.getSentences("Great? gfgfg").size());
        assertEquals("Wrong size.", 0, App.getSentences("").size());
        assertEquals("Wrong size.", 1, App.getSentences("ghjh     ").size());
        assertEquals("Wrong size.", 2, App.getSentences("gdffd... hdfdf").size());

        // Check the getting a certain sentence
        // As words in a sentence are items of a list, I created a string from them to check
        String result = "";
        List<String> sentence = App.getSentences("Fghdhjs. Jkjk 45 " +
                "jojo! \"Hdhdhj\" fh-jfkerfj...").get(2);
        for (int i = 0; i < sentence.size(); i++) {
            result += sentence.get(i) + " ";
        }
        assertEquals("Wrong sentence.", "Hdhdhj fh-jfkerfj ", result);

        assertEquals("Wrong word.", "jojo", App.getSentences("Fghdhjs. Jkjk 45 " +
                "jojo! \"Hdhdhj\" fh-jfkerfj...").get(1).get(1));
    }
}
